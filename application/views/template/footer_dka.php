      <footer class="sticky-footer">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright © <?php echo date('Y') ?><br>
                Waktu Load <strong>{elapsed_time}</strong> detik. <?php echo  (ENVIRONMENT === 'development') ?  'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?>
            </span>
          </div>
        </div>
      </footer>
    </div>
  </div>

  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Anda Yakin?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Klik "Logout" jika ingin mengakhiri sesi aplikasi ini.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="<?php echo base_url('Login_Depkes/logout'); ?>">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- <script src="<?php echo base_url('assets/Template_Bootstrap_Free/vendor/jquery/jquery.min.js'); ?>"></script> -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <script type="text/javascript">
      $(window).load(function() {
          $(".loader").fadeOut("slow");
      });
    </script>

  <script src="<?php echo base_url('assets/Template_Bootstrap_Free/vendor/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>

  <script src="<?php echo base_url('assets/Template_Bootstrap_Free/vendor/jquery-easing/jquery.easing.min.js'); ?>"></script>

  <script src="<?php echo base_url('assets/Template_Bootstrap_Free/vendor/chart.js/Chart.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/Template_Bootstrap_Free/vendor/datatables/jquery.dataTables.js'); ?>"></script>
  <script src="<?php echo base_url('assets/Template_Bootstrap_Free/vendor/datatables/dataTables.bootstrap4.js'); ?>"></script>

  <script src="<?php echo base_url('assets/Template_Bootstrap_Free/js/sb-admin.min.js'); ?>"></script>

  <script src="<?php echo base_url('assets/Template_Bootstrap_Free/js/demo/datatables-demo.js'); ?>"></script>
  <script src="<?php echo base_url('assets/Template_Bootstrap_Free/js/demo/chart-area-demo.js'); ?>"></script>
  
  <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
  <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
  <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
  <script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap4.min.js"></script>
  <script src="<?php echo base_url('assets/datatables_costum/dataTableList.js'); ?>"></script>

</body>

</html>