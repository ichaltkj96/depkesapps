<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class User_Model_dka extends CI_Model {
	
	public function check_credential()
	{
		$username = set_value('username');
		$password = set_value('password');
		
		$hasil = $this->db->where('username',$username)
		->where('password',$password)
		->limit(1)
		->get('dka_data_login');
		
		if($hasil->num_rows() > 0){
			return $hasil->row();
		}else{
			return array();
		}
    }
	    
	public function get_user_dka()
	{
		$hasil=$this->db->get('dka_data_login');
		if($hasil->num_rows() > 0){
			return $hasil->result();
		}else{
			return false;
		}
	}
	
	public function update_dka($id, $master_data)
	{
		$this->db->where('id_login',$id)
		->update('dka_data_login',$master_data);	
	}

	public function find($id)
	{
		$hasil = $this->db->where('id_login',$id)
		->limit(1)
		->get('dka_data_login');
		if($hasil->num_rows() > 0){
			return $hasil->row();
		}else{
			return array();
		}
	}
    
}